ShadowsocksRR
===========

A fast tunnel proxy that helps you bypass firewalls.
* [akkariiin/master]
* [akkariiin/dev]

## [工作原理](https://github.com/shadowsocksrr/shadowsocks-rss/wiki/obfs)

Clinent -> Server 方向
瀏覽器請求（socks5協議） -> ssr客戶端 -> 協議外掛（轉為指定協議） -> 加密 -> 混淆外掛（轉為表面上看起來像http/tls） -> ssr服務端 -> 混淆外掛（分離出加密資料） -> 解密 -> 協議外掛（轉為原協議） -> 轉發目標伺服器

>其中，協議外掛主要用於增加資料完整性校驗，增強安全性，包長度混淆等，混淆外掛主要用於偽裝為其它協議


伺服器 / Server
------
### 安裝 / Install

Debian / Ubuntu:

    apt-get install git python python3
    git clone https://gitlab.com/EthereumD/ShadowsocksRR.git

### 啟動 / Start

move to "~/shadowsocksr/shadowsocks"

    cd ~/shadowsocksr/shadowsocks

then run（建議配置，但是僅python server & client 支持）:

    sudo python server.py -p 443 -k password  -m rc4 -O auth_akarin_spec_a -o tls1.2_ticket_fastauth -d start

or run（ios支持的）

	sudo python server.py -p 443 -k password  -m rc4 -O auth_aes128_sha1 -o tls1.2_ticket_fastauth -d start


>如果要使用 salsa20 或 chacha20 或 chacha20-ietf 算法 看[這裡](https://github.com/shadowsocksr-backup/shadowsocks-rss/wiki/libsodium)

#### 參數選項

##### -m

	- none
	- table

	- rc4-md5
	- rc4-md5-6

OpenSSL Crypto Start

	- aes-128-cbc
	- aes-192-cbc
	- aes-256-cbc
	- aes-128-gcm
	- aes-192-gcm
	- aes-256-gcm
	- aes-128-cfb
	- aes-192-cfb
	- aes-256-cfb
	- aes-128-ofb
	- aes-192-ofb
	- aes-256-ofb
	- aes-128-ctr
	- aes-192-ctr
	- aes-256-ctr
	- aes-128-cfb8
	- aes-192-cfb8
	- aes-256-cfb8
	- aes-128-cfb1
	- aes-192-cfb1
	- aes-256-cfb1
	- camellia-128-cfb
	- camellia-192-cfb
	- camellia-256-cfb
	- cast5-cfb
	- des-cfb
	- idea-cfb
	- rc2-cfb
	- rc4
	- seed-cfb

Sodium Crypto start

	- salsa20
	- chacha20
	- chacha20-ietf
	- xchacha20
	- xsalsa20

##### -o(小)

	- plain
	- verify_deflate
	- http_simple
	- http_simple_compatible(聽說有兼容的容易掛)
	- http_post
	- http_post_compatible(聽說有兼容的容易掛)
	- random_head
	- random_head_compatible(聽說有兼容的容易掛)
	- tls1.2_ticket_auth
    - tls1.2_ticket_auth_compatible
    - tls1.2_ticket_fastauth
    - tls1.2_ticket_fastauth_compatible

##### -O(大)

	- origin
	- auth_sha1_v4
	- auth_sha1_v4_compatible(聽說有兼容的容易掛)
	- auth_aes128_md5 (code看起來怪怪的)
	- auth_aes128_sha1
	- auth_chain_a
	- auth_chain_b
	- auth_chain_c
	- auth_chain_d
	- auth_chain_e
	- auth_chain_f
	- auth_akarin_rand
	- auth_akarin_spec_a




Check all the options via `-h`.

You can also use a configuration file instead (recommend), move to "~/shadowsocksr" and edit the file "user-config.json", then move to "~/shadowsocksr/shadowsocks" again, just run:

    python server.py

To run in the background / 背景運行:

    ./logrun.sh

To stop / 停止運行:

    ./stop.sh

To monitor the log / 查看log文件 :

    ./tail.sh


客戶端 / Client (Broken)
------

* [Windows]
* [Android]
* [python]

License
-------

Copyright 2015 clowwindy

[Android]:https://github.com/shadowsocksrr/shadowsocksr-android/releases
[Windows]:https://github.com/shadowsocksrr/shadowsocksr-csharp/releases
[python]:https://github.com/shadowsocksrr/shadowsocksr/tree/akkariiin/dev
[python/akkariiin/master]:https://github.com/shadowsocksrr/shadowsocksr/tree/akkariiin/master
[python/akkariiin/dev]:https://github.com/shadowsocksrr/shadowsocksr/tree/akkariiin/dev

## SS, SSR, SSRR 參考文獻
[shadowsocksr python 客戶端](https://github.com/EthereumD/shadowsocksr)、
[逗比根据地](https://doub.io)  
